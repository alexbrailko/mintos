export const theme = {
  colors: {
    primary: '#3C668B',
    border: '#8e8a8a',
    bgLight: '#EDEDED',
    bgNormal: '#d2d2d2',
    white: '#fff',
    alert: 'red',
  },
  fontSizes: {
    normal: '1.2em',
  },
  border: {
    radius: '5px',
  },
};
