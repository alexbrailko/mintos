import { render, screen } from '@testing-library/react';
import App from './App';

test('renders CurrencySelector component', () => {
  render(<App />);
  expect(screen.getByTestId('currencySelector')).toBeDefined();
});
