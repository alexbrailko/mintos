import { ComponentType } from 'react';
import { ThemeProvider } from 'styled-components';
import { theme } from './theme';

export function withThemeProvider<P>(WrappedComponent: ComponentType<P>) {
  return (props: P & {}) => {
    return (
      <ThemeProvider theme={theme}>
        <WrappedComponent {...props} />
      </ThemeProvider>
    );
  };
}
