import { render, screen } from '@testing-library/react';
import { withThemeProvider } from '../../helpers';
import { CurrencySelector } from '../CurrencySelector';
import user from '@testing-library/user-event';

const CurrencySelectorWrapped = withThemeProvider(CurrencySelector);

const data = ['EUR', 'USD'];

test('renders CurrencySelector component with data', () => {
  render(<CurrencySelectorWrapped data={data} />);

  expect(screen.getAllByTestId('currencyFieldContainer')).toHaveLength(2);
});

test('adds selected currency to CurrencySelectedField component', () => {
  render(<CurrencySelectorWrapped data={data} />);

  user.click(screen.getAllByTestId('currencyFieldContainer')[0]);
  expect(screen.getAllByTestId('currencySelectedContainer')).toHaveLength(1);
});

test('removes selected currency from CurrencySelectedField component', () => {
  render(<CurrencySelectorWrapped data={data} />);
  user.click(screen.getAllByTestId('currencyFieldContainer')[0]);
  user.click(screen.getByTestId('deselectCurrency'));
  expect(screen.queryByTestId('currencySelectedContainer')).toBeNull();
});
