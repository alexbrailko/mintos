import { render, screen } from '@testing-library/react';
import { withThemeProvider } from '../../helpers';
import user from '@testing-library/user-event';
import { CurrencySelectedField } from '../CurrencySelectedField';

const CurrencySelectedFieldWrapped = withThemeProvider(CurrencySelectedField);

test('expect to render properly and to pass data on click to onDeselect function', () => {
  const onDeselect = jest.fn();
  const currency = 'USD';
  render(
    <CurrencySelectedFieldWrapped
      currency={currency}
      onDeselect={onDeselect}
    />,
  );

  expect(screen.getByTestId('currencySelectedContainer')).toHaveTextContent(
    currency,
  );

  user.click(screen.getByTestId('deselectCurrency'));
  expect(onDeselect).toHaveBeenCalledWith(currency);
});
