import { render, screen } from '@testing-library/react';
import { withThemeProvider } from '../../helpers';
import user from '@testing-library/user-event';
import { CurrencyField } from '../CurrencyField';

const CurrencyFieldWrapped = withThemeProvider(CurrencyField);

test('expect to render properly and to pass data on click to onSelect function', () => {
  const onSelect = jest.fn();
  const currency = 'USD';
  render(
    <CurrencyFieldWrapped
      currency={currency}
      selected={true}
      onSelect={onSelect}
    />,
  );

  expect(screen.getByTestId('currencyText')).toHaveTextContent(currency);

  user.click(screen.getByTestId('currencyFieldContainer'));
  expect(onSelect).toHaveBeenCalledWith(currency, false);
});
