import { FC } from 'react';
import styled from 'styled-components';

type CurrencyFieldType = {
  currency: string;
  onDeselect: (currency: string) => void;
};

export const CurrencySelectedField: FC<CurrencyFieldType> = ({
  currency,
  onDeselect,
}) => {
  return (
    <CurrencySelectedContainer data-testid="currencySelectedContainer">
      {currency}
      <div
        className="unselect"
        onClick={() => onDeselect(currency)}
        data-testid="deselectCurrency"
      >
        <span>X</span>
      </div>
    </CurrencySelectedContainer>
  );
};

const CurrencySelectedContainer = styled.div`
  background-color: ${(props) => props.theme.colors.bgLight};
  color: #000;
  font-size: ${(props) => props.theme.fontSizes.normal};
  padding: 5px;
  text-align: center;
  position: relative;

  .unselect {
    position: absolute;
    width: 17px;
    height: 17px;
    border: 1px solid #000;
    background-color: #000;
    border-radius: 100px;
    top: -5px;
    right: -5px;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;

    &:hover {
      background-color: #fff;
      border: 1px solid #000;

      span {
        color: #000;
      }
    }

    span {
      color: #fff;
      font-size: 13px;
      font-weight: 500;
      position: absolute;
      top: -1px;
      left: 4px;
    }
  }
`;
