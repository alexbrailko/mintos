import { FC } from 'react';
import styled from 'styled-components';

type CurrencyFieldType = {
  currency: string;
  selected: boolean;
  onSelect: (currency: string, selected: boolean) => void;
};

export const CurrencyField: FC<CurrencyFieldType> = ({
  currency,
  selected,
  onSelect,
}) => {
  return (
    <CurrencyFieldContainer
      selected={selected}
      onClick={() => onSelect(currency, !selected)}
      data-testid="currencyFieldContainer"
    >
      <div className="checkbox">{selected && <span>X</span>}</div>
      <div className="currency" data-testid="currencyText">
        {currency}
      </div>
    </CurrencyFieldContainer>
  );
};

const CurrencyFieldContainer = styled.div<{
  selected: boolean;
}>`
  border: 1px solid ${(props) => props.theme.colors.border};
  background-color: ${(props) =>
    props.selected ? props.theme.colors.white : props.theme.colors.bgLight};
  color: ${(props) => props.theme.colors.primary};
  display: flex;
  align-items: center;
  cursor: pointer;
  padding: 2px 4px;

  &:hover {
    background-color: ${(props) =>
      props.selected ? props.theme.colors.white : props.theme.colors.bgNormal};
    transition: background-color 0.3s;
  }

  .checkbox {
    border: 1px solid ${(props) => props.theme.colors.border};
    border-radius: ${(props) => props.theme.border.radius};
    width: 15px;
    height: 15px;
    display: flex;
    align-items: center;
    justify-content: center;

    span {
      color: ${(props) => props.theme.colors.alert};
      font-weight: 500;
    }
  }

  .currency {
    font-size: ${(props) => props.theme.fontSizes.normal};
    font-weight: 500;
    padding-left: 5px;
  }
`;
