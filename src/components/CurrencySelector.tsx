import { FC, useEffect, useState } from 'react';
import styled from 'styled-components';
import { CurrencyField } from './CurrencyField';
import { CurrencySelectedField } from './CurrencySelectedField';

type CurrencySelectorType = {
  data: string[];
};

export const CurrencySelector: FC<CurrencySelectorType> = ({ data }) => {
  const [currencies, setCurrencies] = useState<
    { currency: string; selected: boolean }[]
  >([]);

  useEffect(() => {
    const currenciesData = data.map((c) => {
      return {
        currency: c,
        selected: false,
      };
    });
    setCurrencies(currenciesData);
  }, [data]);

  const onCurrencySelect = (currency: string, selected: boolean) => {
    setCurrencies((current) =>
      current.map((obj) => {
        if (obj.currency === currency) {
          return { currency, selected };
        }
        return obj;
      }),
    );
  };

  const renderCurrencies = () =>
    currencies.map((c) => (
      <CurrencyField
        key={c.currency}
        currency={c.currency}
        selected={c.selected}
        onSelect={onCurrencySelect}
      />
    ));

  const renderSelectedCurrencies = () =>
    currencies.map(
      (c) =>
        c.selected && (
          <CurrencySelectedField
            key={c.currency}
            currency={c.currency}
            onDeselect={(currency) => onCurrencySelect(currency, false)}
          />
        ),
    );

  return (
    <CurrencyContainer data-testid="currencySelector">
      <div className="selectedWrapper">{renderSelectedCurrencies()}</div>
      <div className="currenciesWrapper">{renderCurrencies()}</div>
    </CurrencyContainer>
  );
};

const CurrencyContainer = styled.div`
  border: 1px solid ${(props) => props.theme.colors.border};
  box-shadow: 2px 2px 2px 0px rgb(190 185 185 / 50%);
  border-radius: ${(props) => props.theme.border.radius};
  padding: 10px;
  width: 350px;
  margin-left: auto;
  margin-right: auto;

  .selectedWrapper {
    margin-bottom: 30px;
  }

  .currenciesWrapper,
  .selectedWrapper {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    grid-column-gap: 5px;
    grid-row-gap: 10px;
  }
`;
