export const currencies = [
  'EUR',
  'PLN',
  'GER',
  'DKK',
  'CZK',
  'GBP',
  'SEK',
  'USD',
  'RUB',
] as string[];
