import { CurrencySelector } from './components/CurrencySelector';
import { ThemeProvider } from 'styled-components';
import { currencies } from './data';
import { theme } from './theme';

function App() {
  return (
    <ThemeProvider theme={theme}>
      <div className="container mt-5">
        <CurrencySelector data={currencies} />
      </div>
    </ThemeProvider>
  );
}

export default App;
